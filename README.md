# Data Logger

This package holds everything you need to add Data Logging to your Unity project.
Writes output to a .csv file
There is a base class and an example implementation to log a transform pos, rot, scale
Creates and writes to a 'Logs' folder when used in editor, writes to persistentdatapath when built

## Usage:
- To log a transform, add the 'TransformDataLogger' to an object in the scene and add the target transform in the inspector.
- To log other data, create a new script, instantiate a DataLogger with the name of your log file and the names[] of your columns.
- Use DataLogger.Log() to write your values (string) to the log file.
- TransformDataLogger.cs can be used as an example