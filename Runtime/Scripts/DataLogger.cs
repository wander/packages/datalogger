using System;
using System.IO;
using UnityEngine;

namespace Wander
{
    public class DataLogger
    {
        private StreamWriter writer = null;

        public DataLogger(string pFileName, string[] pColumnNames)
        {
            string _path = CreatePath(pFileName);

            writer = new StreamWriter(_path)
            {
                AutoFlush = true
            };

            Log(pColumnNames);
        }

        private string CreatePath(string pFileName)
        {
#if UNITY_EDITOR
            string _logPath = @"Assets/Logs/";
#else
        string _logPath = Application.persistentDataPath + "/Logs/";
#endif
            Directory.CreateDirectory(_logPath);

            string _timeStamp = DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss");

            return $"{_logPath}{pFileName}_{_timeStamp}.csv";
        }

        // Write given values in the log file
        public void Log(params string[] pValues)
        {
            string _line = "";

            for (int i = 0; i < pValues.Length; ++i)
            {
                // Remove new lines so they don't break csv
                pValues[i] = pValues[i].Replace("\r", "").Replace("\n", "");
                // Do not add semicolon to last data string
                _line += pValues[i] + (i == (pValues.Length - 1) ? "" : ";");
            }

            writer.WriteLineAsync(_line);
        }

        ~DataLogger()
        {
            if (writer != null)
            {
                writer.Flush();
                writer.Close();
                writer = null;
            }
        }
    }
}
