using System;
using System.Collections.Generic;
using UnityEngine;

namespace Wander
{
    public class TransformDataLogger : MonoBehaviour
    {
        [SerializeField] private string fileName = "";
        [SerializeField] private Transform trackedTransform;

        private readonly string[] columnNames = { "CaptureTime (ms)", "Position", "Rotation", "Scale", "DeltaDistance", "TotalDistance", "DeltaRotation (deg)", "TotalRotation (deg)", "Warnings" };

        //Position
        private Vector3 lastPosition;
        private float deltaDistance;
        private float totalDistance;

        //Rotation
        private Quaternion lastRotation;
        private float deltaRotation;
        private float totalRotation;

        public float TotalRotation { get => totalRotation; }

        private DataLogger dataLogger;

        private void Start()
        {
            dataLogger = new DataLogger(fileName, columnNames);

            lastPosition = trackedTransform.position;
            lastRotation = trackedTransform.rotation;
        }

        private void Update()
        {
            //Position
            deltaDistance = Vector3.Distance(lastPosition, trackedTransform.position);
            totalDistance += deltaDistance;
            lastPosition = trackedTransform.position;

            //Rotation
            deltaRotation = Quaternion.Angle(lastRotation, trackedTransform.rotation);
            totalRotation += deltaRotation;
            lastRotation = trackedTransform.rotation;

            LogData();
        }

        private void LogData()
        {
            List<string> _logData = new List<string>();

            // Log time (milliseconds)
            _logData.Add((DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());

            //Base data
            _logData.Add(trackedTransform.position.ToString("F3"));
            _logData.Add(trackedTransform.rotation.ToString("F3"));
            _logData.Add(trackedTransform.lossyScale.ToString("F3"));

            //Position
            _logData.Add(deltaDistance.ToString("F3"));
            _logData.Add(totalDistance.ToString("F3"));

            //Rotation
            _logData.Add(deltaRotation.ToString("F3"));
            _logData.Add(TotalRotation.ToString("F3"));

            //Warnings
            _logData.Add(DataWarning());

            dataLogger.Log(_logData.ToArray());
        }

        //This is just for data readability
        private string DataWarning()
        {
            string _dataWarning = "";

            //Check position
            if (deltaDistance > 1.0f)
            {
                _dataWarning += "DeltaDistance > 1.0f ";
            }

            //Check rotation
            if (deltaRotation > 10.0f)
            {
                _dataWarning += "DeltaRotation > 10.0f ";
            }

            return _dataWarning;
        }
    }
}